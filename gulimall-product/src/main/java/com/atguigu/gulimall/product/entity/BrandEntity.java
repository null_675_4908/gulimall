package com.atguigu.gulimall.product.entity;

import com.atguigu.common.valid.AddGroup;
import com.atguigu.common.valid.ListValue;
import com.atguigu.common.valid.UpdateGroup;
import com.atguigu.common.valid.UpdateStatusGroup;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;
import org.hibernate.validator.constraints.URL;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.constraints.*;

/**
 * 品牌
 * 
 * @author lsx
 * @email 290006982@qq.com
 * @date 2020-10-07 20:32:20
 */
@Data
@TableName("pms_brand")
public class BrandEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 品牌id
	 */
	@NotNull(message = "修改时，必须制定品牌id", groups = {UpdateGroup.class})
	@Null(message = "新增时，不需要id", groups = {AddGroup.class})
	@TableId
	private Long brandId;
	/**
	 * 品牌名
	 */
	@NotBlank(message = "品牌名不能为空", groups = { UpdateGroup.class, AddGroup.class })
	private String name;
	/**
	 * 品牌logo地址
	 * 效验规则
	 * 1. 新增的时候：不能为空
	 * 2. 修改的时候：可以为空，如果不为空就效验是否为url
	 */
	@NotEmpty(groups = {AddGroup.class})
	@URL(message = "logo地址不合法", groups = {AddGroup.class, UpdateGroup.class})
	private String logo;
	/**
	 * 介绍
	 */
	private String descript;
	/**
	 * 显示状态[0-不显示；1-显示]
	 */
	@ListValue(vals = {0, 1}, groups = {AddGroup.class, UpdateStatusGroup.class})// 自定义效验器
	private Integer showStatus;
	/**
	 * 检索首字母
	 */
	@NotEmpty// 未指定分组，效验规则不生效
	@Pattern(regexp = "^[a-zA-Z]$", message = "首字母必须是字母")// 自定义验证规则// 未指定分组，效验规则不生效
	private String firstLetter;
	/**
	 * 排序
	 */
	@NotNull// 未指定分组，效验规则不生效
	@Min(value = 0, message = "排序必须大于0")// 未指定分组，效验规则不生效
	private Integer sort;
}
