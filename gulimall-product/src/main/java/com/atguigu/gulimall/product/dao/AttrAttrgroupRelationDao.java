package com.atguigu.gulimall.product.dao;

import com.atguigu.gulimall.product.entity.AttrAttrgroupRelationEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 属性&属性分组关联
 * 
 * @author lsx
 * @email 290006982@qq.com
 * @date 2020-10-07 20:32:20
 */
@Mapper
public interface AttrAttrgroupRelationDao extends BaseMapper<AttrAttrgroupRelationEntity> {
    // 自己写sql
    // 一定要给参数取好名字，方便mapper通过名字获取参数。@Param("entities")
    void deleteBatchRelation(@Param("entities") List<AttrAttrgroupRelationEntity> entities);
}
