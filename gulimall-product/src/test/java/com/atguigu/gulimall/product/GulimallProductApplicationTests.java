package com.atguigu.gulimall.product;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClient;
import com.aliyun.oss.OSSClientBuilder;
import com.atguigu.gulimall.product.entity.BrandEntity;
import com.atguigu.gulimall.product.service.BrandService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@SpringBootTest
class GulimallProductApplicationTests {
    @Autowired
    BrandService brandService;

    @Autowired
    OSSClient ossClient;

    @Test
    void contextLoads() {
        System.out.println(brandService.count() + "-------------");
    }

    @Test
    void saveOne(){
        BrandEntity brandEntity = new BrandEntity();
        brandEntity.setName("华为");

        System.out.println("插入成功------------" + brandService.save(brandEntity));
    }

    @Test
    void updateById(){
        BrandEntity brandEntity = new BrandEntity();
        brandEntity.setName("小米");
        brandEntity.setBrandId(1L);


        System.out.println("插入成功------------" + brandService.updateById(brandEntity));
    }


    @Test
    void get(){
        List<BrandEntity> list = brandService.list(new QueryWrapper<BrandEntity>().eq("brand_id", 1L));
        list.forEach((item) -> {
            System.out.println(item + "------------------");
        });
    }

    @Test
    void testUpload() throws FileNotFoundException {
        // Endpoint以杭州为例，其它Region请按实际情况填写。
        String endpoint = "http://oss-cn-shanghai.aliyuncs.com";
        // 云账号AccessKey有所有API访问权限，建议遵循阿里云安全最佳实践，创建并使用RAM子账号进行API访问或日常运维，请登录 https://ram.console.aliyun.com 创建。
        String accessKeyId = "LTAI4Fy5LsW2c5invo9wQvTd";
        String accessKeySecret = "0luVYNL65HGFyO1mrNdvuDDB4tJPrt";

        // 创建OSSClient实例。
        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);

        // 上传文件流。
        InputStream inputStream = new FileInputStream("C:\\Users\\Administrator\\Desktop\\1.jpg");
        String myBucketName = "lsx-gulimall";
        String fileName = "1.jpg";// 服务器上存储的文件名
        ossClient.putObject(myBucketName, fileName, inputStream);

        // 关闭OSSClient。
        ossClient.shutdown();

        System.out.println("---------上传完成---------");
    }


    @Test
    void testUploadStarter() throws FileNotFoundException {
//        // Endpoint以杭州为例，其它Region请按实际情况填写。
//        String endpoint = "http://oss-cn-shanghai.aliyuncs.com";
//        // 云账号AccessKey有所有API访问权限，建议遵循阿里云安全最佳实践，创建并使用RAM子账号进行API访问或日常运维，请登录 https://ram.console.aliyun.com 创建。
//        String accessKeyId = "LTAI4Fy5LsW2c5invo9wQvTd";
//        String accessKeySecret = "0luVYNL65HGFyO1mrNdvuDDB4tJPrt";
//
//        // 创建OSSClient实例。
//        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);

        // 上传文件流。
        InputStream inputStream = new FileInputStream("C:\\Users\\Administrator\\Desktop\\1.jpg");
        String myBucketName = "lsx-gulimall";
        String fileName = "1.jpg";// 服务器上存储的文件名
        ossClient.putObject(myBucketName, fileName, inputStream);

        // 关闭OSSClient。
        ossClient.shutdown();

        System.out.println("---------上传完成---------");
    }


    @Test
    void arrayToListxxxxx(){
        List<Long> arrayList = new ArrayList<>();
        arrayList.add(1L);
        arrayList.add(2L);
        arrayList.add(3L);

        Long[] array = arrayList.toArray(new Long[3]);

        System.out.println("----------------->" + Arrays.asList(array));
    }
}
