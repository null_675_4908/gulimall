package com.atguigu.gulimall.order.dao;

import com.atguigu.gulimall.order.entity.OrderEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单
 * 
 * @author lsx
 * @email 290006982@qq.com
 * @date 2020-10-08 15:39:58
 */
@Mapper
public interface OrderDao extends BaseMapper<OrderEntity> {
	
}
