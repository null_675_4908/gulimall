package com.atguigu.common.valid;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.HashSet;
import java.util.Set;

public class ListValueConstraintValidator implements ConstraintValidator<ListValue, Integer> {
    private Set set = new HashSet();


    // 初始化方法
    @Override
    public void initialize(ListValue constraintAnnotation) {
        int[] vals = constraintAnnotation.vals();// 标注的时候给定的值
        for(int val: vals){
            set.add(val);
        }
    }

    // 判断是否效验成功
    @Override
    public boolean isValid(Integer value/*需要效验的值*/, ConstraintValidatorContext constraintValidatorContext) {

        return set.contains(value);
    }
}
