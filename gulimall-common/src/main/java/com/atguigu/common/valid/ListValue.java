package com.atguigu.common.valid;


import java.lang.annotation.*;
import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.constraints.NotEmpty;

// 标示这是一个 自定义注解
@Documented
// Constraint: 约束
// 指定效验器使用哪个注解进行效验，默认为空需要在初始化的时候指定
@Constraint( validatedBy = {ListValueConstraintValidator.class} )// 使用我们自己写的效验器进行效验(可以指定多个)
// 可以标注的位置
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE, ElementType.CONSTRUCTOR, ElementType.PARAMETER, ElementType.TYPE_USE})
// 效验时机
@Retention(RetentionPolicy.RUNTIME)
// 可重复注解
// @Repeatable(NotEmpty.List.class)
public @interface ListValue {
    /*------规范 start------*/
    // 使用 ValidationMessages.properties 配置文件
    String message() default "{com.atguigu.common.valid.ListValue.message}";// 使用配置文件的 com.atguigu.common.valid.ListValue.message 的内容进行提示
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
    /*------规范 end------*/

    int[] vals() default {};
}
