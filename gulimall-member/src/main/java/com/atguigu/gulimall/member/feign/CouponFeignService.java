package com.atguigu.gulimall.member.feign;

import com.atguigu.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient("gulimall-coupon")// gulimall-coupon 注册中心注册的服务名
public interface CouponFeignService {
    /**
     * /coupon/coupon
     * /member/list
     */
    @RequestMapping("/coupon/coupon/member/list")
    public R membercoupons();
}
