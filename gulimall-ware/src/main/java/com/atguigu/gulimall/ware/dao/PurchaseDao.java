package com.atguigu.gulimall.ware.dao;

import com.atguigu.gulimall.ware.entity.PurchaseEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 采购信息
 * 
 * @author lsx
 * @email 290006982@qq.com
 * @date 2020-10-08 15:47:20
 */
@Mapper
public interface PurchaseDao extends BaseMapper<PurchaseEntity> {
	
}
